import sys, os; sys.path.insert(0, f"{os.getcwd()}/../swarm-lib-python/")

from flask import Flask, request, jsonify
from led import Led
from swarm_lib import Provider

app = Flask(__name__)
led = Led(11)
provider = Provider(
    description_file="./description.jsonld",
    policies_file="./policies.json",
    keys_file="./keys.json"
)

@app.route("/led-service/led", methods=["PUT"])
@provider.enforce_authorization
def put_led():
    led.toggle()
    return jsonify({"led_status": led.state})

if __name__ == "__main__":
    if provider.join_swarm():
        app.run(host="0.0.0.0", port=provider.port(), debug=True)
    else:
        print("Could not join the Swarm Network")

