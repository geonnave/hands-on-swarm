
try:
    import K9.GPIO as GPIO
except Exception as e:
    print(e)
    class GPIO():
        HIGH = 1
        LOW = 0
        OUT = 1
        BOARD = "labrador"
        def setmode(board):
            pass
        def setup(pin, value):
            pass
        def output(pin, value):
            pass

class Led():
    def __init__(self, pin):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(pin,GPIO.OUT)
        self.pin = pin
        self.state = GPIO.LOW

    def toggle(self):
        if self.state == GPIO.LOW:
            self.on()
        else:
            self.off()
        return self.state

    def on(self):
        print("Ligando LED")
        self.state = GPIO.HIGH
        GPIO.output(self.pin, self.state)

    def off(self):
        print("Desligando LED")
        self.state = GPIO.LOW
        GPIO.output(self.pin, self.state)

if __name__ == "__main__":
    import time

    led = Led(11)
    led.on()
    time.sleep(1)
    led.off()

