import sys, os; sys.path.insert(0, f"{os.getcwd()}/../swarm-lib-python/")

from swarm_lib import Consumer

consumer = Consumer(keys_file="keys.json")

query = {
    "@type": "swarm:Led",
    "operation": {"@type": "swarm:UpdateOperation"},
    "usageDuration": 30, "minCandidates": 1
}

# led_services = consumer.discover(query)

led_executor = consumer.get_executor(query)
if led_executor.contract_providers():
    responses, feedbacks = led_executor.execute()

    led_executor.compute_and_send_reputation(feedbacks)
